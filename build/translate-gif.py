#!/usr/bin/python
 
import gimpfu
import math
import os
import sys

from sys import platform
if platform == "linux" or platform == "linux2":
  import yaml
elif platform == "darwin":
  # osx hack for yaml
  sys.path.append('/usr/local/lib/python2.7/site-packages')
  import yaml
elif platform == "win32":
  import yaml

def getBaseName(templatePath):
    templateFile = os.path.basename(templatePath)
    baseName = templateFile.rsplit(".",1)[0]
    return baseName 

def getText(langRef):
    file = os.path.join(languageDir, langRef, textFileName)
    with open(file) as f:
        texts = yaml.load(f)
    return texts

def getJustification(justify):
    if justify == 'left':
      return TEXT_JUSTIFY_LEFT
    if justify == 'center':
      return TEXT_JUSTIFY_CENTER
    if justify == 'right':
      return TEXT_JUSTIFY_RIGHT
    else:
      return TEXT_JUSTIFY_LEFT

def mergeText(image, layer, text, size, xOffset=0, yOffset=0, rotate=0, justify='left', font='Learn to Fly'):
    layer.visible = True
    textLayer = pdb.gimp_text_fontname(image, layer, xOffset, yOffset, text, 0, True, size, PIXELS, font)

    if justify != 'left':
      pdb.gimp_text_layer_set_justification(textLayer, getJustification(justify))

    if rotate != 0:
      pdb.gimp_item_transform_rotate(textLayer, math.radians(rotate), True, 0, 0)

    mergedLayer = pdb.gimp_image_merge_visible_layers(image, 0)
    mergedLayer.visible = False
    return mergedLayer

def addText(filename):
    img = pdb.gimp_file_load(filename, filename)
    texts = getText(i18n)

    # bg layer
    bgLayer = img.layers[bgIndex]
    bgItems = [
      'title',
      'healthcare',
      'x-axis',
      'y-axis'
    ]

    # woman layer
    womanLayer = img.layers[womanIndex]
    womanItems = [
      'woman-1',
      'woman-2'
    ]

    # man layer
    manLayer = img.layers[manIndex]
    manItems = [
      'man'
    ]

    for key, value in texts.iteritems():
      if key in bgItems:
        bgLayer = mergeText(img, bgLayer, **value)

      elif key in manItems:
        manLayer = mergeText(img, manLayer, **value)

      elif key in womanItems:
        womanLayer = mergeText(img, womanLayer, **value)

    # safe the i18n version
    baseName = getBaseName(filename)
    i18nName = cacheDir + "/" + baseName + "-" + i18n + ".xcf"
    pdb.gimp_xcf_save(0, img, None, i18nName, i18nName)
    pdb.gimp_image_delete(img)
    return i18nName

def export(filename):
    img = pdb.gimp_file_load(filename, filename)
    pdb.script_fu_animstack_process_all(img)
    pdb.script_fu_flatten_layer_groups(img)
    pdb.gimp_image_convert_indexed(img, NO_DITHER, MAKE_PALETTE, 128, False, True, "")

    baseName = getBaseName(filename)

    outName = cacheDir + "/" + baseName + "-" + "out" + ".xcf" 
    pdb.gimp_xcf_save(0, img, None, outName, outName)

    gifName = os.path.join(releaseDir, baseName + ".gif")
    print("Generating " + gifName)
    pdb.file_gif_save(img, None, gifName ,gifName, 0, 1, 200 , 0)
    pdb.gimp_image_delete(img)
    return gifName

# get params from env
i18n = os.environ['I18N']
templateFile = os.environ['XCFFILE']

# config filename
textFileName = 'text.yaml'
# set dirs
cacheDir = '.cache'
templateDir = 'templates'
languageDir = 'languages'
releaseDir = 'releases'

bgIndex=11
womanIndex=12
manIndex=13

if not os.path.exists(cacheDir):
  os.mkdir(cacheDir)

i18nFilePath = addText(templateFile)
gifFilePath = export(i18nFilePath)

pdb.gimp_quit(1)